# ==============================================================================
#
#   LIBRAIRIES
#
# ==============================================================================

import pandas as p
import numpy as np
import os
import sys
import time

# ==============================================================================
#
#   Parameters
#
# ==============================================================================
# Define parameter values.
dropouts = [.15, .2, .25, .3, .35, .4]
learning_rates = [1e-4, 5e-4, 1e-3, 5e-3, 1e-2, 5e-2]
optimizers = ['adadelta', 'proxadagrad', 'adagrad', 'adam']

# Optimal configuration for 'Death from disease'.
OUTCOME = 'death-from-disease'
ID = '2d0142'
ILAYER = '22'
RSEED = '691985397' # R9
# Optimal configuration for 'Disease progression'.
# OUTCOME = 'progression'
# ID = '2d1014'
# ILAYER = '33'
# RSEED = '198916456' # R3

# ==============================================================================
#
#   Main
#
# ==============================================================================
if __name__ == '__main__':

    # Start by opening the lists of experiments.
    EXP_LIST_FILE = sys.argv[1]
    experiments = p.read_csv(EXP_LIST_FILE, sep = '\t', header = None).as_matrix()

    # Replicate tag
    RTAG = sys.argv[2]

    # Output folder
    OUTFOLDER = sys.argv[3]

    # For each sample.
    for exp in experiments:

        # We only do some of the runs.
        if exp[0] != ID:
            continue
        if exp[1] != OUTCOME:
            continue

        for dropout in dropouts:
            for optimizer in optimizers:
                for learning_rate in learning_rates:
                    # Prepare tags.
                    tagP = ID + ' ' + str(ILAYER) + ' ' + str(dropout) + ' ' + optimizer + ' ' + str(learning_rate) + ' ' + str(RTAG) + ' ' + RSEED + ' ' + OUTFOLDER
                    tagF = ID + '_' + str(ILAYER) + '_' + str(dropout) + '_' + optimizer + '_' + str(learning_rate) + '_' + str(RTAG)
                    logO =  OUTFOLDER + '/logs/' + tagF + '.out'
                    logE =  OUTFOLDER + '/logs/' + tagF + '.err'
                    # Run the command if the log file does not exists yet.
                    if not os.path.isfile(logO):
                        cmd = 'python3 ./DNN_CAMDEEP.py ' + tagP + ' > ' + logO + ' 2> ' + logE + ' &'
                        print('[' + tagP + '] Job started.')
                        print(tagF, file=sys.stderr)
                        os.system(cmd)
                        time.sleep(20)
                    # Otherwise, log and go straight to the next run.
                    else:
                        print('[' + tagP + '] Not started (log file exists).')
