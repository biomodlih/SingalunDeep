###############################################################################
## Classification of CAMDEEP samples (clinical outcomes).
## by P.Nazarov, L-C.Tranchevent
##
## 2018-05-26 Update to run using the same data structure than the DNN
##	(this time for CAMDEEP).
## 2017-05-03 MCC added, configuration adapted to CAMDA project, aldo added
##      standardization of the input data.
## 2017-05-02 batch processing is added
## 2017-04-28 initial version
###############################################################################

# =============================================================================
#   Environment
# =============================================================================
rm(list = ls())
library(e1071)

# Get parameters
args = commandArgs(trailingOnly=TRUE)
run_id = args[1]
svm_params = strsplit(args[2], ',')[[1]]
valid_ids = args[3]
endpoint = args[4]
data.folder = args[5]

kernel.type = svm_params[1]
cost = as.numeric(svm_params[2])
gamma = as.numeric(svm_params[3])

stdinput = TRUE

# =============================================================================
#   Functions
# =============================================================================

# Function that builds the confusion matrix starting from two tables
# representing the real and predicted classes.
getConfusionMatrix = function(gr, gr.pred) {
    n = max(c(gr, gr.pred))
    Tab = matrix(nc = n, nr = n)
    rownames(Tab) = paste('pred', 1:n, sep = ".")
    colnames(Tab) = paste('group', 1:n, sep = ".")
    for (i in 1:n)
        for (j in 1:n){
            Tab[i,j] = sum((gr.pred == i) & (gr== j))
    }
    return(Tab)
}

# Function that computes the classification accuracy from a confusion matrix.
getAccuracy = function(CM) {
    x = 0
    for (i in 1:ncol(CM))
        x = x + CM[i,i]
    return(x / sum(CM))
}

# Function that computes the classification accuracy from a confusion matrix.
getBalancedAccuracy = function(CM) {
    x = 0
    for (i in 1:ncol(CM)) {
        y = 0
        for (j in 1:ncol(CM)) {
            y = y + CM[j,i]
        }
        x = x + (CM[i,i] / y)
    }
    return(x / 2)
}

# Function that computes the Matthew correlation coefficient from a
# confusion matrix.
getMCC = function(CM) {

    # We first compute the denominator as:
    #   D = sqrt((TP + FP)(TP + FN)(TN + FP)(TN + FN))
    preD = (CM[1,1] + CM[2,1]) * (CM[1,1] + CM[1,2]) * (CM[2,2] + CM[2,1]) * (CM[2,2] + CM[1,2])

    # If this pre-denominator is 0 because any of the sum is 0, we set it to
    # one (MCC convention). Then we use square root.
    if (preD == 0) preD = 1
    D = sqrt(preD)

    # We compute the numerator, and return the fraction.
    N = (CM[1,1] * CM[2,2] - CM[2,1] * CM[1,2])
    return(N / D)
}

# =============================================================================
#   Main
# =============================================================================

# We log our progress...
cat(paste0('[SVM_', kernel.type,'] Doing', run_id, ".\n"))
flush.console()

# We import the training data.
Train = list()
Train$X = as.matrix(read.table(paste(data.folder, run_id, '_data_train.tsv', sep = ''),
    sep = "\t", row.names = 1))
Train$Y = as.integer(read.table(paste(data.folder, 'clinical_', endpoint, '_train.tsv',
    sep = ''))[[3]])
names(Train$Y) = rownames(Train$X)

# We import the evaluation data.
Eval = list()
Eval$X = as.matrix(read.table(paste(data.folder, run_id, '_data_eval.tsv', sep = ''),
    sep = "\t", row.names = 1))
Eval$Y = as.integer(read.table(paste(data.folder, 'clinical_', endpoint , '_eval.tsv',
    sep = ''))[[3]])
names(Eval$Y)  = rownames(Eval$X)

# We adapt the class labels if needed.
# In case, we have (0,1), we want (1,2)
if (max(Train$Y) == 1 | max(Eval$Y) == 1) {
    Train$Y = Train$Y + 1
    Eval$Y = Eval$Y + 1
}
# In case, we still have (0,X), we want (1,X) (with X >1).
if (min(Train$Y) == 0 | min(Eval$Y) == 0) {
    Train$Y = Train$Y + 1
    Eval$Y = Eval$Y + 1
}
Train$Y = factor(Train$Y)
Eval$Y = factor(Eval$Y)

# We standardize the data (mean = 0, sd = 1).
if (stdinput == TRUE) {
    Train$X = scale(Train$X)
    Eval$X = scale(Eval$X)
}
Train$X[is.na(Train$X)] <- 0
Eval$X[is.na(Eval$X)] <- 0

# We train a SVM model using only training data.
model.svm = svm(Train$X, Train$Y, kernel = kernel.type, cost = cost, gamma = gamma)

# Function to make prediction given one model and the id of the other run to be used for validation.
mypredict = function(model.svm, valid_run_id, endpoint) {

    # We import the validation data.
    Valid = list()
    Valid$X = as.matrix(read.table(paste(data.folder, valid_run_id, '_data_valid.tsv', sep = ''),
        sep = "\t", row.names = 1))

    # Real labels.
    validy_filename = paste(data.folder, valid_run_id, '_clinical_', endpoint, '_valid.tsv', sep = '')
    Valid$Y = as.integer(read.table(validy_filename)[[3]])
    names(Valid$Y)  = rownames(Valid$X)

    # We adapt the class labels if needed.
    # In case, we have (0,1), we want (1,2)
    if (max(Valid$Y) == 1) {
        Valid$Y = Valid$Y + 1
    }
    # In case, we still have (0,X), we want (1,X) (with X >1).
    if (min(Valid$Y) == 0) {
        Valid$Y = Valid$Y + 1
    }
    Valid$Y = factor(Valid$Y)

    # We standardize the data (mean = 0, sd = 1).
    if (stdinput == TRUE) {
        Valid$X = scale(Valid$X)
    }
    Valid$X[is.na(Valid$X)] <- 0

    # We assess the performance on the validation data.
    Valid$P = predict(model.svm, Valid$X)
    Valid.CM = getConfusionMatrix(Valid$Y, Valid$P)
    acc = getAccuracy(Valid.CM)
    bacc = getBalancedAccuracy(Valid.CM)
    mcc = getMCC(Valid.CM)

    # We save the predictions.
    out.pred_file = paste(data.folder, 'SVM_', run_id, '_', valid_run_id, '_', endpoint, '_SVMpreds.tsv', sep='')

    # We write down the results in a text file (-1 to go from {1,2} to {0,1}).
    write.table(as.numeric(as.character(Valid$P))-1, file = out.pred_file, sep = "\t", col.names = FALSE, row.names = FALSE, quote = FALSE)

    # We log the performance.
    cat(paste('Validation_', valid_run_id, ':', bacc), "\n")
    flush.console()
}

# End of main: validate using the function predict on all valids ids.
for (valid_data in strsplit(valid_ids, ',')[[1]]) {
  valid_run_id = strsplit(valid_data, '-')[[1]]
  mypredict(model.svm, valid_run_id[1], endpoint)
}
