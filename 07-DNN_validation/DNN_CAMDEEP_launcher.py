# ==============================================================================
#
#   LIBRAIRIES
#
# ==============================================================================

import pandas as p
import numpy as np
import os
import sys
import time

# ==============================================================================
#
#   Main
#
# ==============================================================================
if __name__ == '__main__':

    # Start by opening the lists of experiments.
    EXP_LIST_FILE = sys.argv[1]
    experiments = p.read_csv(EXP_LIST_FILE, sep = '\t', header = None).as_matrix()

    # Output folder
    OUTFOLDER = sys.argv[2]

    # For each sample.
    for exp in experiments:

        # Stored parameters
        ilayer = exp[4]
        rtag = exp[5]
        rseed = exp[6]
        valid_ids = exp[7]

        tagP = exp[0] + ' ' + str(ilayer) + ' ' + str(rtag) + ' ' + str(rseed) + ' ' + valid_ids + ' ' + exp[1] + ' ' + OUTFOLDER
        tagF = exp[0] + '_' + str(ilayer) + '_' + str(rtag)
        logO =  OUTFOLDER + '/logs/' + tagF + '.out'
        logE =  OUTFOLDER + '/logs/' + tagF + '.err'
        # Run the command if the log file does not exists yet.
        if not os.path.isfile(logO):
            cmd = 'python3 ./DNN_CAMDEEP.py ' + tagP + ' > ' + logO + ' 2> ' + logE + ' &'
            print('[' + tagP + '] Job started.')
            print(tagF, file=sys.stderr)
            os.system(cmd)
            time.sleep(20)
        # Otherwise, log and go straight to the next run.
        else:
            print('[' + tagP + '] Not started (log file exists).')
