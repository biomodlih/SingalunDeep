# ==============================================================================
#
#   LIBRAIRIES
#
# ==============================================================================

import pandas as p
import numpy as np
import os
import sys
import time

# ==============================================================================
#
#   Main
#
# ==============================================================================
if __name__ == '__main__':

    # Start by opening the lists of experiments.
    EXP_LIST_FILE = sys.argv[1]
    experiments = p.read_csv(EXP_LIST_FILE, sep = '\t', header = None).as_matrix()

    # Output folder
    OUTFOLDER = sys.argv[2]

    # For each sample.
    for exp in experiments:

        # Stored parameters
        valid_ids = exp[7]
        rf_params = exp[9]

        tagP = exp[0] + ' ' + rf_params + ' ' + valid_ids + ' ' + exp[1] + ' ' + OUTFOLDER
        tagF = exp[0] + '_RF'
        logO =  OUTFOLDER + '/logs/' + tagF + '.out'
        logE =  OUTFOLDER + '/logs/' + tagF + '.err'
        # Run the command if the log file does not exists yet.
        if not os.path.isfile(logO):
            cmd = 'Rscript --vanilla ./RF_CAMDEEP.r ' + tagP + ' > ' + logO + ' 2> ' + logE + ' &'
            print('[' + tagP + '] Job started.')
            print(tagF, file=sys.stderr)
            os.system(cmd)
            time.sleep(2)
        # Otherwise, log and go straight to the next run.
        else:
            print('[' + tagP + '] Not started (log file exists).')
