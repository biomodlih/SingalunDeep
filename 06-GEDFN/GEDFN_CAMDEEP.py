# ============================================================================
#
#	LIBRARIES
#
# ============================================================================
import numpy as np
import os
# To adapt and switch between CPUs and GPUs.
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf
from sklearn.utils import shuffle
from sklearn import metrics
from random import seed
import time
import sys
import pandas as p

# ============================================================================
#
#	PARAMETERS
#
# ============================================================================
## PARAMS: run_id, rtag, outfolder.
run_id = sys.argv[1]
rtag = sys.argv[8]
rseed = sys.argv[9]
outfolder = sys.argv[10]

# File names.
train_data_file = outfolder + run_id + '_train.tsv'
eval_data_file = outfolder + run_id + '_eval.tsv'
valid_data_file = outfolder + run_id + '_valid.tsv'
network_adjacency_file = outfolder + 'netAdj_' + run_id + '.tsv'
results_file = outfolder + 'logs/' + run_id + '_featimportance.tsv'
file_tag = sys.argv[1] + '_' + sys.argv[8] + '_' + sys.argv[2] + sys.argv[3] + '_' + sys.argv[4] + '_' + sys.argv[5] + '_' + sys.argv[6] + '_' + sys.argv[7]
pred_file_name = outfolder + file_tag + '_GEDFNpreds.tsv'

# Hyper-parameters and settings.
droph1 = False
display_step = 1
n_classes = 2
# Command line parameters.
n_hidden_2 = int(sys.argv[2])
n_hidden_3 = int(sys.argv[3])
learning_rate = float(sys.argv[4])
L2 = sys.argv[5] # Either "True" or considered as representing False.
training_epochs = int(sys.argv[6])
batch_size = int(sys.argv[7])

# We set the random seed.
tf.set_random_seed=int(rseed)

# ==============================================================================
#
#   FUNCTIONS
#
# ==============================================================================

# Function to get the confusion matrix from the correct labels and the
# predictions.
def getCM(labels, pred_labels):
  vals = np.array([0,1]) # Force it to be {0,1}
  n = len(vals)
  tab = np.ndarray(shape=(n,n))
  for vi in vals:
    for vj in vals:
      tab[vi, vj] = len(labels[(pred_labels == vi) & (labels == vj)])
  return(tab)

# Function that computes the classification balanced accuracy from a
# confusion matrix.
def getBalancedAccuracy(CM):
  x = 0
  for i in np.arange(len(CM)):
    y = 0
    for j in np.arange(len(CM)):
      y = y + CM[j,i]
    x = x + CM[i,i]/y
  return(.5 * x)

# ============================================================================
#
#	MAIN
#	(with code adapted from the GEDFN repository:
#	https://github.com/yunchuankong/GEDFN/).
#
# ============================================================================

# Re-init TF.
tf.reset_default_graph()

# Load in data.
train_data = np.loadtxt(train_data_file, dtype=float, delimiter="\t")
eval_data = np.loadtxt(eval_data_file, dtype=float, delimiter="\t")
valid_data = np.loadtxt(valid_data_file, dtype=float, delimiter="\t")
net_adj = np.loadtxt(network_adjacency_file, dtype=int, delimiter="\t")

# Reformat the data to extract the labels (clinical outcomes).
# 	[n columns of data, weights, labels].
train_label_vec = np.array(train_data[:,-1], dtype=int)
eval_label_vec = np.array(eval_data[:,-1], dtype=int)
valid_label_vec = np.array(valid_data[:,-1], dtype=int)
train_exp = np.array(train_data[:, :-1])
eval_exp = np.array(eval_data[:, :-1])
valid_exp = np.array(valid_data[:, :-1])

# Function to refine the labels (from one column to two columns).
def refineLabels(label_vec):
    labels = []
    for l in label_vec:
        if l == 1:
            labels.append([0,1])
        else:
            labels.append([1,0])
    labels = np.array(labels, dtype=int)
    return labels

# Refine the labels.
train_labels = refineLabels(train_label_vec)
eval_labels = refineLabels(eval_label_vec)
valid_labels = refineLabels(valid_label_vec)

# Constant limit for feature selection.
gamma_c = 50
gamma_numerator = np.sum(net_adj, axis=0)
gamma_denominator = np.sum(net_adj, axis=0)
gamma_numerator[np.where(gamma_numerator > gamma_c)] = gamma_c

# Neural network architecture (additional layers).
n_hidden_1 = np.shape(net_adj)[0]
n_features = np.shape(net_adj)[1]

# Initiate logs.
loss_rec = np.zeros([training_epochs, 1])
eval = np.zeros([training_epochs, 2])

# Function that define the network based on the defined architecture.
def multilayer_perceptron(x, weights, biases, keep_prob):
    # First layer.
    layer_1 = tf.add(tf.matmul(x, tf.multiply(weights['h1'], net_adj)), biases['b1'])
    layer_1 = tf.nn.relu(layer_1)
    if droph1:
        layer_1 = tf.nn.dropout(layer_1, keep_prob=keep_prob)
    # Second layer.
    layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
    layer_2 = tf.nn.relu(layer_2)
    layer_2 = tf.nn.dropout(layer_2, keep_prob=keep_prob)
    # Third layer.
    layer_3 = tf.add(tf.matmul(layer_2, weights['h3']), biases['b3'])
    layer_3 = tf.nn.relu(layer_3)
    layer_3 = tf.nn.dropout(layer_3, keep_prob=keep_prob)
    # Output layer.
    out_layer = tf.matmul(layer_3, weights['out']) + biases['out']
    return out_layer

# We prepare the inputs and model parameters.
x = tf.placeholder(tf.float32, [None, n_features])
y = tf.placeholder(tf.int32, [None, n_classes])
keep_prob = tf.placeholder(tf.float32)
lr = tf.placeholder(tf.float32)

# Weights and biases for all neurons.
weights = {
    'h1': tf.Variable(tf.truncated_normal(shape=[n_features, n_hidden_1], stddev=0.1)),
    'h2': tf.Variable(tf.truncated_normal(shape=[n_hidden_1, n_hidden_2], stddev=0.1)),
    'h3': tf.Variable(tf.truncated_normal(shape=[n_hidden_2, n_hidden_3], stddev=0.1)),
    'out': tf.Variable(tf.truncated_normal(shape=[n_hidden_3, n_classes], stddev=0.1))

}
biases = {
    'b1': tf.Variable(tf.zeros([n_hidden_1])),
    'b2': tf.Variable(tf.zeros([n_hidden_2])),
    'b3': tf.Variable(tf.zeros([n_hidden_3])),
    'out': tf.Variable(tf.zeros([n_classes]))
}

# Construct model
pred = multilayer_perceptron(x, weights, biases, keep_prob)

# Define loss and optimizer.
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
if L2 == 'True':
    reg = tf.nn.l2_loss(weights['h1']) + tf.nn.l2_loss(weights['h2']) + \
          tf.nn.l2_loss(weights['h3']) + tf.nn.l2_loss(weights['out'])
    cost = tf.reduce_mean(cost + 0.01 * reg)

optimizer = tf.train.AdamOptimizer(learning_rate=lr).minimize(cost)

# Evaluation.
correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
y_score = tf.nn.softmax(logits=pred)
var_left = tf.reduce_sum(tf.abs(tf.multiply(weights['h1'], net_adj)), 0)
var_right = tf.reduce_sum(tf.abs(weights['h2']), 1)
var_importance = tf.add(tf.multiply(tf.multiply(var_left, gamma_numerator), 1./gamma_denominator), var_right)

# Nor run the model with the data using a TF session.
with tf.Session() as sess:
    # Initialization of the parameters.
    sess.run(tf.global_variables_initializer())
    total_batch = int(np.shape(train_exp)[0] / batch_size)
    # Training cycle.
    for epoch in range(training_epochs):
        avg_cost = 0.
        x_tmp, y_tmp = shuffle(train_exp, train_labels)
        # Loop over all batches
        for i in range(total_batch-1):
            batch_x, batch_y = x_tmp[i*batch_size:i*batch_size+batch_size], y_tmp[i*batch_size:i*batch_size+batch_size]
            _, c= sess.run([optimizer, cost], feed_dict={x: batch_x, y: batch_y, keep_prob: 0.9, lr: learning_rate})
            # Compute average loss
            avg_cost += c / total_batch
        del x_tmp
        del y_tmp
        # Display logs per epoch step
        if epoch % display_step == 0:
            loss_rec[epoch] = avg_cost
            acc, y_s = sess.run([accuracy, y_score], feed_dict={x: eval_exp, y: eval_labels, keep_prob: 1})
            auc = metrics.roc_auc_score(eval_labels, y_s)
            eval[epoch] = [acc, auc]
            print ("Epoch:", '%d' % (epoch+1), "cost =", "{:.9f}".format(avg_cost), "Evaluation accuracy:", round(acc,3), " Evaluation auc:", round(auc,3))
        # Early stopping during training.
        ##if avg_cost <= 0.1:
        ##    print("Early stopping.")
        ##    break
    # Testing the model (valid data).
    acc, y_s = sess.run([accuracy, y_score], feed_dict={x: valid_exp, y: valid_labels, keep_prob: 1})
    auc = metrics.roc_auc_score(valid_labels, y_s)
    y_s_ref = np.argmax(y_s, axis = 1)
    # We save the predictions for the statistical analysis.
    np.savetxt(pred_file_name, y_s_ref, delimiter="\t", fmt="%d")
    valid_labels_ref = p.DataFrame(valid_labels).iloc[:,1].as_matrix()
    valid_CM = getCM(valid_labels_ref, y_s_ref)
    valid_bACC = getBalancedAccuracy(valid_CM)
    var_imp = sess.run([var_importance])
    var_imp = np.reshape(var_imp, [n_features])
    print("*****=====", "Validation accuracy: ", acc, " Validation auc: ", auc, "=====*****")
    print("*****=====", "Validation balanced accuracy: ", valid_bACC, "=====*****")

np.savetxt(results_file, var_imp, delimiter=",")
