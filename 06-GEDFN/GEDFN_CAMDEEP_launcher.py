# ==============================================================================
#
#   LIBRAIRIES
#
# ==============================================================================

import pandas as p
import numpy as np
import os
import sys
import time

# ==============================================================================
#
#   Parameters
#
# ==============================================================================
# Define parameter values.
DATASETS = ['marray_death', 'both_prog']
HIDDENS = [[64, 16], [16, 4]]
LRATES = [1e-4, 1e-2]
L2NORM = ['True', 'False']
EPOCHS = [100, 1000]
BATCHS = [8,32]

# ==============================================================================
#
#   Main
#
# ==============================================================================
if __name__ == '__main__':

    # Replicate tag and seed.
    RTAG = sys.argv[1]
    RSEED = sys.argv[2]

    # Output folder
    OUTFOLDER = sys.argv[3]

    # For each dataset and then configuration.
    for run_id in DATASETS:
        for arch in HIDDENS:
            for lr in LRATES:
                for l2 in L2NORM:
                    for e in EPOCHS:
                        for b in BATCHS:
                            tagP = run_id + ' ' + str(arch[0]) + ' ' + str(arch[1]) + ' ' + str(lr) + ' ' + l2 + ' ' + str(e) + ' ' + str(b) + ' ' + RTAG + ' ' + RSEED + ' ' + OUTFOLDER
                            tagF = run_id + '_' + str(arch[0]) + '_' + str(arch[1]) + '_' + str(lr) + '_' + l2 + '_' + str(e) + '_' + str(b) + '_' + RTAG
                            logO =  OUTFOLDER + '/logs/' + tagF + '.out'
                            logE =  OUTFOLDER + '/logs/' + tagF + '.err'
                            # Run the command if the log file does not exists yet.
                            if not os.path.isfile(logO):
                                cmd = 'python3 ./GEDFN_CAMDEEP.py ' + tagP + ' > ' + logO + ' 2> ' + logE
                                print('[' + tagP + '] Job started.')
                                os.system(cmd)
                                time.sleep(1)
                            # Otherwise, log and go straight to the next run.
                            else:
                                print('[' + tagP + '] Not started (log file exists).')
