# ==============================================================================
#
#   LIBRAIRIES
#
# ==============================================================================

import pandas as p
import numpy as np
import os
import sys
import time

# ==============================================================================
#
#   Parameters
#
# ==============================================================================
# Optimal configuration for 'Death from disease'.
OUTCOME = 'death-from-disease'
ID = '2d0142'
ILAYER = '22'

# Optimal configuration for 'Disease progression'.
# OUTCOME = 'progression'
# ID = '2d1014'
# ILAYER = '33'

# ==============================================================================
#
#   Main
#
# ==============================================================================
if __name__ == '__main__':

    # Start by opening the lists of experiments.
    EXP_LIST_FILE = sys.argv[1]
    experiments = p.read_csv(EXP_LIST_FILE, sep = '\t', header = None).as_matrix()

    # Replicate infos
    seedfile = sys.argv[2]
    with open(seedfile) as f:
        seeds = dict(x.rstrip().split(None, 1) for x in f)

    # Output folder
    OUTFOLDER = sys.argv[3]

    # For each sample.
    for exp in experiments:

        # We only do some of the runs.
        if exp[0] != ID:
            continue
        if exp[1] != OUTCOME:
            continue

        for rtag in sorted(seeds.keys()):
            rseed = seeds[rtag]

            # We prepare the tags.
            tagP = ID + ' ' + str(ILAYER) + ' ' + str(rtag) + ' ' + str(rseed) + ' ' + OUTFOLDER
            tagF = ID + '_' + str(ILAYER) + '_' + str(rtag)
            logO =  OUTFOLDER + '/logs/' + tagF + '.out'
            logE =  OUTFOLDER + '/logs/' + tagF + '.err'
            # Run the command if the log file does not exists yet.
            if not os.path.isfile(logO):
                cmd = 'python3 ./DNN_CAMDEEP.py ' + tagP + ' > ' + logO + ' 2> ' + logE + ' &'
                print('[' + tagP + '] Job started.')
                print(tagF, file=sys.stderr)
                os.system(cmd)
                time.sleep(20)
            # Otherwise, log and go straight to the next run.
            else:
                print('[' + tagP + '] Not started (log file exists).')
