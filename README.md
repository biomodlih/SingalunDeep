# SingalunDeep

## 1. Introduction
This repository contains the code associated with the following manuscript:
> Tranchevent L., Azuaje F.. Rajapakse J.C.
> **A deep neural network approach to predicting clinical outcomes of neuroblastoma patients**
> *Manuscript submitted (2019)*
