###############################################################################
## Classification of CAMDEEP samples (clinical outcomes).
## by P.Nazarov, L-C.Tranchevent
##
## 2018-05-26 Update to run using the same data structure than the DNN
##	(this time for CAMDEEP).
## 2017-05-03 MCC added, configuration adapted to CAMDA project, aldo added
##      standardization of the input data.
## 2017-05-02 batch processing is added
## 2017-04-28 initial version
###############################################################################

# =============================================================================
#   Environment
# =============================================================================
rm(list = ls())
library(randomForest)

# Get parameters
args = commandArgs(trailingOnly=TRUE)
run_id = args[1]
run_replicate_id = args[2]
run_random_seed = args[3]
endpoint = args[4]
data.folder = args[5]

# Input the file that will contain the results.
out.result_file = paste(data.folder, 'oRF_', run_id, '_', run_replicate_id, '.tsv', sep='')
stdinput = TRUE

# We set the random seed (will be used by RF).
set.seed(as.numeric(run_random_seed))

# =============================================================================
#   Functions
# =============================================================================

# Function that builds the confusion matrix starting from two tables
# representing the real and predicted classes.
getConfusionMatrix = function(gr, gr.pred) {
    n = max(c(gr, gr.pred))
    Tab = matrix(nc = n, nr = n)
    rownames(Tab) = paste('pred', 1:n, sep = ".")
    colnames(Tab) = paste('group', 1:n, sep = ".")
    for (i in 1:n)
        for (j in 1:n){
            Tab[i,j] = sum((gr.pred == i) & (gr== j))
    }
    return(Tab)
}

# Function that computes the classification accuracy from a confusion matrix.
getAccuracy = function(CM) {
    x = 0
    for (i in 1:ncol(CM))
        x = x + CM[i,i]
    return(x / sum(CM))
}

# Function that computes the classification accuracy from a confusion matrix.
getBalancedAccuracy = function(CM) {
    x = 0
    for (i in 1:ncol(CM)) {
        y = 0
        for (j in 1:ncol(CM)) {
            y = y + CM[j,i]
        }
        x = x + (CM[i,i] / y)
    }
    return(x / 2)
}

# Function that computes the Matthew correlation coefficient from a
# confusion matrix.
getMCC = function(CM) {

    # We first compute the denominator as:
    #   D = sqrt((TP + FP)(TP + FN)(TN + FP)(TN + FN))
    preD = (CM[1,1] + CM[2,1]) * (CM[1,1] + CM[1,2]) * (CM[2,2] + CM[2,1]) * (CM[2,2] + CM[1,2])

    # If this pre-denominator is 0 because any of the sum is 0, we set it to
    # one (MCC convention). Then we use square root.
    if (preD == 0) preD = 1
    D = sqrt(preD)

    # We compute the numerator, and return the fraction.
    N = (CM[1,1] * CM[2,2] - CM[2,1] * CM[1,2])
    return(N / D)
}

# =============================================================================
#   Main
# =============================================================================

# We log our progress...
cat(paste0('[oRF] Doing', run_id, ', ', run_replicate_id, ".\n"))
flush.console()

# We import the training data.
Train = list()
Train$X = as.matrix(read.table(paste(data.folder, run_id, '_data_train.tsv', sep = ''),
    sep = "\t", row.names = 1))
Train$Y = as.integer(read.table(paste(data.folder, 'clinical_', endpoint, '_train.tsv',
    sep = ''))[[3]])
names(Train$Y) = rownames(Train$X)

# We import the evaluation data.
Eval = list()
Eval$X = as.matrix(read.table(paste(data.folder, run_id, '_data_eval.tsv', sep = ''),
    sep = "\t", row.names = 1))
Eval$Y = as.integer(read.table(paste(data.folder, 'clinical_', endpoint , '_eval.tsv',
    sep = ''))[[3]])
names(Eval$Y)  = rownames(Eval$X)

# We import the validation data.
Valid = list()
Valid$X = as.matrix(read.table(paste(data.folder, run_id, '_data_valid.tsv', sep = ''),
    sep = "\t", row.names = 1))
Valid$Y = as.integer(read.table(paste(data.folder, 'clinical_', endpoint , '_valid.tsv',
    sep = ''))[[3]])
names(Valid$Y)  = rownames(Valid$X)

# We adapt the class labels if needed.
# In case, we have (0,1), we want (1,2)
if (max(Train$Y) == 1 | max(Eval$Y) == 1 | max(Valid$Y) == 1) {
    Train$Y = Train$Y + 1
    Eval$Y = Eval$Y + 1
    Valid$Y = Valid$Y + 1
}
# In case, we still have (0,X), we want (1,X) (with X >1).
if (min(Train$Y) == 0 | min(Eval$Y) == 0 | min(Valid$Y) == 0) {
    Train$Y = Train$Y + 1
    Eval$Y = Eval$Y + 1
    Valid$Y = Valid$Y + 1
}
Train$Y = factor(Train$Y)
Eval$Y = factor(Eval$Y)
Valid$Y = factor(Valid$Y)

# We standardize the data (mean = 0, sd = 1).
if (stdinput == TRUE) {
    Train$X = scale(Train$X)
    Eval$X = scale(Eval$X)
    Valid$X = scale(Valid$X)
}
Train$X[is.na(Train$X)] <- 0
Eval$X[is.na(Eval$X)] <- 0
Valid$X[is.na(Valid$X)] <- 0

# We train a RF model using only training data.
model.rf = NA # randomForest(Train$X, Train$Y, ntree = 1000)

# Optimization of the RF model.
nbtrees = c(100,250,500,1000,2500,5000,10000)
i <- 1

# We prepare the variables that will contain the classification results.
Res = data.frame(Id = character(length(nbtrees)), Replicate = "",
    Algo = "", nbT = "", Eval.bacc = NA, Valid.acc = NA, Valid.bacc = NA, Valid.mcc = NA,
    stringsAsFactors = FALSE)

for (nbt in nbtrees) {
    model.rf = randomForest(Train$X, Train$Y, ntree = nbt)

    # We assess the performance on the validation data.
    Valid$P = predict(model.rf, Valid$X)
    Valid.CM = getConfusionMatrix(Valid$Y, Valid$P)
    # We put the parameter values in the result structure.
    Res$Id[i] = run_id
    Res$Replicate[i] = run_replicate_id
    Res$Algo[i] = 'oRF'
    Res$nbT[i] = nbt
    Res$Valid.acc[i] = getAccuracy(Valid.CM)
    Res$Valid.bacc[i] = getBalancedAccuracy(Valid.CM)
    Res$Valid.mcc[i] = getMCC(Valid.CM)
    i <- i + 1
}

# We write down the results in a text file.
write.table(Res, file = out.result_file, sep = "\t", col.names = TRUE, row.names = FALSE, quote = FALSE)
