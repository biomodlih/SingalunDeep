# ==============================================================================
#
#   LIBRAIRIES
#
# ==============================================================================

import pandas as p
import numpy as np
import os
import sys
import time

# ==============================================================================
#
#   Main
#
# ==============================================================================
if __name__ == '__main__':

    # Start by opening the lists of experiments.
    EXP_LIST_FILE = sys.argv[1]
    experiments = p.read_csv(EXP_LIST_FILE, sep = '\t', header = None).as_matrix()

    # Replicate tag and seed.
    RTAG = sys.argv[2]
    RSEED = sys.argv[3]

    # Output folder
    OUTFOLDER = sys.argv[4]

    # For each sample.
    for exp in experiments:
        tagP = exp[0] + ' ' + str(RTAG) + ' ' + str(RSEED) + ' ' + exp[1] + ' ' + OUTFOLDER
        tagF = 'oRF_' + exp[0] + '_' + str(RTAG)
        logO =  OUTFOLDER + '/logs/' + tagF + '.out'
        logE =  OUTFOLDER + '/logs/' + tagF + '.err'
        # Run the command if the log file does not exists yet.
        if not os.path.isfile(logO):
            cmd = 'Rscript --vanilla ./oRF_CAMDEEP.r ' + tagP + ' > ' + logO + ' 2> ' + logE + ' &'
            print('[' + tagP + '] Job started.')
            print(tagF, file=sys.stderr)
            os.system(cmd)
            time.sleep(1) # RF is fast.
        # Otherwise, log and go straight to the next run.
        else:
            print('[' + tagP + '] Not started (log file exists).')
