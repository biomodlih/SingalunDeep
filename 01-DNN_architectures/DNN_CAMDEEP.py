# ==============================================================================
#
#   LIBRAIRIES
#
# ==============================================================================

import os
# To adapt and switch between CPUs and GPUs.
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf
import argparse
import sys
import csv
import time
import numpy as np
import pandas as p

from official.utils.arg_parsers import parsers
from official.utils.logs import hooks_helper
from official.utils.misc import model_helpers

# ==============================================================================
#
#   VARIABLES AND PARAMETERS
#
# ==============================================================================

# DNN
train_epochs = 1000
epochs_between_evals = 10
batch_size = 32
hooks = ['LoggingTensorHook']
stop_threshold = None
DROPOUT_DEFAULT = 0.3
LR_DEFAULT = 1e-3

# DATA
data_dir = ''
model_dir = ''
NB_SAMPLES_TRAIN = 249
NB_SAMPLES_EVAL = 125
NB_SAMPLES_VALID = 124

# ==============================================================================
#
#   FUNCTIONS
#
# ==============================================================================

# Function to get the confusion matrix from the correct labels and the
# predictions.
def getCM(labels, pred_labels):
  vals = np.array([0,1]) # Force it to be {0,1}
  n = len(vals)
  tab = np.ndarray(shape=(n,n))
  for vi in vals:
    for vj in vals:
      tab[vi, vj] = len(labels[(pred_labels == vi) & (labels == vj)])
  return(tab)

# Function that computes the classification balanced accuracy from a
# confusion matrix.
def getBalancedAccuracy(CM):
  x = 0
  for i in np.arange(len(CM)):
    y = 0
    for j in np.arange(len(CM)):
      y = y + CM[j,i]
    x = x + CM[i,i]/y
  return(.5 * x)

# ==============================================================================
#   Build the DNN estimator for the provided data.
#	(adapted from the TensorFlow deep and wide tutorial).
# ==============================================================================
def build_estimator_DNN(model_dir, nbF, run_id, ilayer, layer, rtag, rseed):
  # We define the tage for this run (based on input parameters).
  tag = str(run_id) + '_l' + str(ilayer) + '_' + rtag

  # Parameters for the DNN classifier.
  # 1. Feature columns (aka features) as tf feature_column objects.
  # We use all columns (feature selection happenend before).
  deep_columns = [tf.feature_column.numeric_column('f' + str(i+1)) for i in range(nbF-2)]
  # 2. Number of layers and number of neurons per layer.
  hidden_units = layer
  # 3. Optimizer used to train the model.
  # Here, we use ADAM but without parameter tunning.
  optimizer = tf.train.AdamOptimizer(learning_rate = LR_DEFAULT)
  # 4. Activation function (default value).
  activation_fn = tf.nn.relu
  # 5. Dropout (set our own  default value - because we have not enough instances).
  dropout = DROPOUT_DEFAULT
  # 6. Loss reduction (SUM, for consistency).
  loss_reduction = tf.losses.Reduction.SUM
  #7. Weights for fair accuracy computation.
  weight_column = 'f' + str(nbF-1)
  # Model creation itself.
  return tf.estimator.DNNClassifier(
    model_dir=model_dir + tag,
    feature_columns=deep_columns,
    hidden_units=hidden_units,
    activation_fn=activation_fn,
    dropout=dropout,
    optimizer=optimizer,
    loss_reduction=loss_reduction,
    weight_column=weight_column,
    config=tf.estimator.RunConfig(tf_random_seed=int(rseed))
  )

# ==============================================================================
#   Generate an input function for the Estimator.
#	(adapted from the TensorFlow deep and wide tutorial).
# ==============================================================================
def input_fn(data_file, num_epochs, shuffle, batch_size, COLS, COLDEFS, fclass):
  def parse_csv(value):
    print('Parsing', data_file)
    columns = tf.decode_csv(value, field_delim='\t', record_defaults=COLDEFS)
    features = dict(zip(COLS, columns))
    labels = features.pop(fclass)
    return features, labels
  dataset = tf.data.TextLineDataset(data_file)
  if shuffle:
    dataset = dataset.shuffle(buffer_size=NB_SAMPLES_TRAIN)
  dataset = dataset.map(parse_csv, num_parallel_calls=5)
  dataset = dataset.repeat(num_epochs)
  dataset = dataset.batch(batch_size)
  return dataset

# ==============================================================================
#   Train function
#	(adapted from the TensorFlow deep and wide tutorial).
# ==============================================================================
def train(run_id, ilayer, layer, rtag, rseed):

  # We prepare the data.
  print('[DBG] run_id = ' + str(run_id))
  train_file = os.path.join(data_dir, run_id + '_data_train_4tfw.tsv')
  eval_file = os.path.join(data_dir, run_id + '_data_eval_4tfw.tsv')
  valid_file = os.path.join(data_dir, run_id + '_data_valid_4tfw.tsv')

  # Get the number of features.
  nbF = 0
  with open(train_file) as f:
    reader = csv.reader(f, delimiter='\t')
    nbF = len(next(reader))

  # Build the column names and specs.
  _CSV_COLUMNS = ['f' + str(i+1) for i in range(nbF)]
  _CSV_COLUMN_DEFAULTS = [[0.5] for i in range(nbF)]
  fclass = 'f' + str(nbF)

  # We build the model.
  model = build_estimator_DNN(model_dir, nbF, run_id, ilayer, layer, rtag, rseed)

  # Train and evaluate the model every `flags.epochs_between_evals` epochs.
  def train_input_fn():
    return input_fn(train_file, epochs_between_evals, True, batch_size, _CSV_COLUMNS, _CSV_COLUMN_DEFAULTS, fclass)

  def eval_input_fn():
    return input_fn(eval_file, 1, False, NB_SAMPLES_EVAL, _CSV_COLUMNS, _CSV_COLUMN_DEFAULTS, fclass)

  def valid_input_fn():
    return input_fn(valid_file, 1, False, NB_SAMPLES_VALID, _CSV_COLUMNS, _CSV_COLUMN_DEFAULTS, fclass)

  # Default hooks.
  train_hooks = hooks_helper.get_train_hooks(
      hooks, batch_size=batch_size,
      tensors_to_log={})

  # Train and evaluate the model every `epochs_between_evals` epochs.
  for n in range(train_epochs // epochs_between_evals):

    # Model training.
    model.train(input_fn=train_input_fn, hooks=train_hooks)

    # Model evaluation.
    results = model.evaluate(input_fn=eval_input_fn)

    # Display evaluation metrics
    print('Results at epoch', (n + 1) * epochs_between_evals)
    print('-' * 60)
    for key in sorted(results):
      print('%s: %s' % (key, results[key]))

  # Validate the model.
  model_preds = model.predict(input_fn = valid_input_fn)
  return model_preds

# ==============================================================================
#
#   Main
#
# ==============================================================================
if __name__ == '__main__':
  tf.logging.set_verbosity(tf.logging.INFO)

  # Define parameter values (35 different architectures).
  # { 0- 3} L1_layers = [[1], [2], [4], [8]]
  # { 4- 9} L2_layers = [[8,8], [8,4], [8,2], [4,4], [4,2], [2,2]]
  # {10-19} L3_layers = [[8,8,8], [8,8,4], [8,8,2], [8,4,4], [8,4,2], [8,2,2], [4,4,4], [4,4,2], [4,2,2], [2,2,2]]
  # {20-34} L4_layers = [[8,8,8,8], [8,8,8,4], [8,8,8,2], [8,8,4,4], [8,8,4,2], [8,8,2,2], [8,4,4,4], [8,4,4,2], [8,4,2,2], [8,2,2,2], [4,4,4,4], [4,4,4,2], [4,4,2,2], [4,2,2,2], [2,2,2,2]]
  layers = [[1], [2], [4], [8], [8,8], [8,4], [8,2], [4,4], [4,2], [2,2], [8,8,8], [8,8,4], [8,8,2], [8,4,4], [8,4,2], [8,2,2], [4,4,4], [4,4,2], [4,2,2], [2,2,2], [8,8,8,8], [8,8,8,4], [8,8,8,2], [8,8,4,4], [8,8,4,2], [8,8,2,2], [8,4,4,4], [8,4,4,2], [8,4,2,2], [8,2,2,2], [4,4,4,4], [4,4,4,2], [4,4,2,2], [4,2,2,2], [2,2,2,2]]

  # Define the data and model folders.
  data_dir = sys.argv[5]
  model_dir = sys.argv[5]

  # Train, evaluate the model and get the predictions on the validation set.
  model_preds = train(sys.argv[1], int(sys.argv[2]), layers[int(sys.argv[2])], sys.argv[3], sys.argv[4])

  # Refine the predictions made by the model.
  valid_pred_results = [model_preds.__next__() for i in range(NB_SAMPLES_VALID)]
  valid_pred_labels = np.array([i['class_ids'][0] for i in valid_pred_results])

  # We get the truth values from the files for the validation set.
  valid_file = os.path.join(data_dir, sys.argv[1] + '_data_valid_4tfw.tsv')
  valid_true_labels = p.read_csv(valid_file, sep = '\t', index_col = False, header = None).as_matrix()[:,-1]

  # We compute the CM and the bACC.
  valid_CM = getCM(valid_true_labels, valid_pred_labels)
  valid_bACC = getBalancedAccuracy(valid_CM)

  # We log the bACC.
  print('Validation_bACC:' + str(valid_bACC), file=sys.stdout)
